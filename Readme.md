# Project description
## SoftDrinksStock
* This project is an example project for university on Microservices.
* In this project we store softdrinks and we manage stoc, we can list the soft drink inventory. The implementation is not a production implementation.

## Components:
* Spring server
* MySQL server 

The MySQL server connection is set to connect to Kubernetes.
MySQL database, tables creation script can be found in mysql_scripts.txt .

## Endpoints:
* The version=1 version endpoints are hardcoded.

1. /api/drinks?version=1  - list all soft drinks  - GET request
 parameters: no parameters

2. /api/drinks/{id}?version=1 - list a specific soft drink identified by id - GET request
 parameters: {id} is the id of the soft drink

3. /api/drink?version=1 - add a new drink to the inventory  - POST request
 parameters: JSON soft drink

4. /api/stocks?version=1 - return true if the requested drinks are available in the requested quantity - POST request
 parameters: list of drink id and quantity pairs
 This endpoint is called by Laszlo Balasko's project which can be found at: https://bitbucket.org/balaskol/softdrinkshop/src/master
