package edu.ubb.softdrink.stock.service;

import edu.ubb.softdrink.stock.model.SoftDrinkModel;
import org.springframework.data.repository.CrudRepository;

public interface SoftDrinkRepository extends CrudRepository<SoftDrinkModel, Integer> {

}
