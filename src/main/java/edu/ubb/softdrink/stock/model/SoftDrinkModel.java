package edu.ubb.softdrink.stock.model;

import javax.persistence.*;

@Entity
@Table(name="products")
public class SoftDrinkModel {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected int id;
    protected String name;
    protected String producer;
    protected float price;
    protected int stock;

    public SoftDrinkModel() {
    }

    public SoftDrinkModel(String name, String producer, float price, int stock) {
        this.name = name;
        this.producer = producer;
        this.price = price;
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "SoftDrink{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", producer='" + producer + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                '}';
    }

    public String toStockResponseString() {
        return "{" +
                "\"id\":" + id +
                ", \"price\":" + price +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
