package edu.ubb.softdrink.stock.controller;

import edu.ubb.softdrink.stock.dto.StockRequestDto;
import edu.ubb.softdrink.stock.model.SoftDrinkModel;
import edu.ubb.softdrink.stock.service.SoftDrinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class SoftDrinkController {

    @Autowired
    private SoftDrinkRepository softDrinkRepository;

    /**
     * Versioning with Parameters
     * http://localhost:8080/api/stocks?version=1
     *
     * @return
     */
    @GetMapping(path="/api/drinks", params = "version=1")
    public Iterable<SoftDrinkModel> getAllV1 () {
        return softDrinkRepository.findAll();
    }

    @GetMapping(path="/api/drinks/{id}", params = "version=1")
    public SoftDrinkModel getModelById (@PathVariable int id) {
        Optional<SoftDrinkModel> drink = softDrinkRepository.findById(id);
        if (drink.isPresent()) return drink.get();
            else return null;
    }

    @PostMapping(path="/api/drink")
    public void createNewModel (@RequestBody SoftDrinkModel drink) {
        softDrinkRepository.save(drink);
    }

    @PostMapping(path="/api/stocks", params = "version=1")
    public String getStockById (@RequestBody List<StockRequestDto> items) {

        Float result = new Float(0);

        SoftDrinkModel drink;

        for (StockRequestDto item : items) {
            drink = softDrinkRepository.findById(item.getId()).get();

            if (drink != null && drink.getStock() >= item.getQty()) {
                result += drink.getPrice();
            } else {
                return "0";
            }
        }

        for (StockRequestDto item : items) {
            drink = softDrinkRepository.findById(item.getId()).get();

            if (drink != null && drink.getStock() >= item.getQty()) {
                drink.setStock(drink.getStock() - item.getQty());
                softDrinkRepository.save(drink);
            }
        }

        return result.toString();
    }


}
