package edu.ubb.softdrink.stock.dto;

public class StockRequestDto {
    protected int id;
    protected int qty;

    public StockRequestDto(int id, int qty) {
        this.id = id;
        this.qty = qty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
