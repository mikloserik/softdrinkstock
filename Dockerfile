FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/stock-2.0.0.jar
ADD ${JAR_FILE} stock-2.0.0.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/stock-2.0.0.jar"]
